# Docker environment for _Finance Interface_ project

Simply place your clone of _Finance Interface_ project in **finance** directory and type _./run.sh_ in console.

## Requirements:
- Docker **^1.7.1**
- Docker Compose **^1.8.1**

## Commands:
- **./build.sh** - builds containers
- **./run.sh** - starts docker environment
- **./xrun.sh** - starts docker environment with Xdebug
- **./exec.sh** - started from container directory runs commands from this container environment, for example _../exec.sh php bin/console c:c_ started from **biere** directory will clear cache using php7 version (does not matter which php version - if any - is installed locally at your machine)
