
FROM php:7.0-apache

ENV BUILD_DEPS \
    build-essential \
    libicu-dev \
    libldap2-dev \
    libsasl2-dev \
    libxml2-dev \
    libzip-dev \
    zlib1g-dev \
    apache2-dev

RUN apt-get update && apt-get install -y --no-install-recommends \
        git \
        libmcrypt-dev \
        $BUILD_DEPS \
    && apt-mark manual \
        'libicu[0-9][0-9]' \
        'libzip[0-9]' \
    && rm -r /var/lib/apt/lists/* \
    && docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ \
    && docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd \
    && docker-php-ext-install -j$(nproc) \
        bcmath \
        calendar \
        intl \
        ldap \
        opcache \
        pdo_mysql \
        soap \
        zip \
        mcrypt

RUN yes | pecl install apcu apcu_bc-beta \
    && docker-php-ext-enable apcu

RUN yes | pecl install xdebug

COPY mod_rpaf /tmp/mod_rpaf
RUN (cd /tmp/mod_rpaf && make && make install) \
    && rm -r /tmp/mod_rpaf

RUN apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
        $BUILD_DEPS \
    && apt-get clean \
    && rm -rf /tmp/*

RUN curl -L 'https://phar.phpunit.de/phpunit.phar' -o /usr/local/bin/phpunit \
    && chmod +x /usr/local/bin/phpunit
RUN curl -L 'https://getcomposer.org/installer' -o /tmp/composer-setup.php \
    && php /tmp/composer-setup.php -- --install-dir=/usr/local/bin --filename=composer \
    && rm /tmp/composer-setup.php

RUN mkdir /var/www/.composer
RUN chown www-data /var/www/.composer
RUN chmod -R 775 /var/www/.composer

RUN a2enmod rewrite

COPY php.ini /usr/local/etc/php/conf.d/php.ini
COPY xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini
COPY rpaf.conf /etc/apache2/mods-enabled/rpaf.conf
COPY rpaf.load /etc/apache2/mods-enabled/rpaf.load
RUN ln -s /etc/apache2/mods-available/headers.load /etc/apache2/mods-enabled/headers.load
COPY headers.conf /etc/apache2/mods-enabled/headers.conf
RUN sed -i -e 's:DocumentRoot .*$:DocumentRoot /var/www/html/web:' /etc/apache2/sites-available/000-default.conf

COPY umask-wrapper.sh /usr/local/bin/
COPY apache2-run.sh /usr/local/bin/
RUN usermod -u 1000 www-data

RUN echo '#!/bin/bash\nphp bin/console $*' > /usr/bin/dev
RUN chmod +x /usr/bin/dev
RUN echo '#!/bin/bash\nphp bin/console --env=prod $*' > /usr/bin/prod
RUN chmod +x /usr/bin/prod

#ENTRYPOINT ["umask-wrapper.sh"]
CMD ["apache2-run.sh"]

#RUN apt-get update && apt-get install -y \
#        libapache2-mod-security2 \
#    && a2enmod security2
#COPY security2.conf /etc/apache2/mods-enabled/security2.conf

